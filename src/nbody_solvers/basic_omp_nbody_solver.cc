#include "basic_omp_nbody_solver.h"

basic_omp_nbody_solver::basic_omp_nbody_solver(long n, long n_steps, long output_freq, float delta_t, particle* init, bool anim, long n_thr):
    smp_basic_nbody_solver(n,n_steps,output_freq,delta_t,init,anim), parallel_solver(n_thr)
{
    //ctor
}

void basic_omp_nbody_solver::run() {
#   ifndef NO_OUTPUT
    output_state();
#    endif
    long step;
    long part;
    float_vector* forces = this->forces;
    long n_thr = this->n_thr;
    float delta_t = this->delta_t;
    long n = this->n;
    long n_steps = this->n_steps;
    long output_freq = this->output_freq;
    float time = this->time;
#    pragma omp parallel num_threads(n_thr) default(none) \
        shared(forces, n_thr, delta_t, n, n_steps, output_freq) \
        private(step, part, time)
    for (step = 1; step <= n_steps; step++) {
        time = step*delta_t;
//      memset(forces, 0, n*sizeof(float_vector));
#       pragma omp for
        for (part = 0; part < n; part++)
            compute_forces(part, forces);
#       pragma omp for
        for (part = 0; part < n; part++)
            update_part(part);
#       ifndef NO_OUTPUT
#       pragma omp single
        if (step % output_freq == 0)
            output_state();
#       endif
    }

#   ifndef NO_OUTPUT
    output_state();
#   endif

    this->time = n_steps*delta_t;
}

basic_omp_nbody_solver::~basic_omp_nbody_solver()
{
    //dtor
}

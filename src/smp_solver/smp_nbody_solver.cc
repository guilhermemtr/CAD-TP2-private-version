#include "smp_nbody_solver.h"

smp_nbody_solver::smp_nbody_solver(long n, long n_steps, long output_freq, float delta_t, particle* init, bool anim):
    n_body(n, n_steps, output_freq, delta_t, init, anim)
{
    forces = (float_vector *) calloc(n,sizeof(float_vector));
}

void smp_nbody_solver::update_part(long part) {
    float fact = delta_t/particles[part].mass;

    __yal_log("Before update of %ld:\n", part);
    __yal_log("   Position  = (%.3e, %.3e)\n", particles[part].pos[X], particles[part].pos[Y]);
    __yal_log("   Velocity  = (%.3e, %.3e)\n", particles[part].velocity[X], particles[part].velocity[Y]);
    __yal_log("   Net force = (%.3e, %.3e)\n", forces[part][X], forces[part][Y]);

    particles[part].pos[X] += delta_t * particles[part].velocity[X];
    particles[part].pos[Y] += delta_t * particles[part].velocity[Y];
    particles[part].velocity[X] += fact * forces[part][X];
    particles[part].velocity[Y] += fact * forces[part][Y];

    __yal_log("Position of %ld = (%.3e, %.3e), Velocity = (%.3e,%.3e)\n",
            part, particles[part].pos[X], particles[part].pos[Y],
                particles[part].velocity[X], particles[part].velocity[Y]);
//  particles[part].pos[X] += delta_t * particles[part].velocity[X];
//  particles[part].pos[Y] += delta_t * particles[part].velocity[Y];
}

smp_nbody_solver::~smp_nbody_solver()
{
    free(forces);
}

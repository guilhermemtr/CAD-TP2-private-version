#include "gpu_info.h"

gpu_info::gpu_info() {
    cudaDeviceProp props;
	handle(cudaGetDeviceProperties(&props, 0));
	this->setup_threads_per_block(props.major, props.minor, props.multiProcessorCount);
}

void gpu_info::setup_threads_per_block(int major, int minor, int nr_sm) {
	switch(major) {
		case 2:
			switch(minor) {
				case 0:
					cuda_cores_per_sm = 32;
					fp_cores_per_sm = 4;
					warp_schedulers_per_sm = 2;
				break;
				case 1:
					cuda_cores_per_sm = 48;
					fp_cores_per_sm = 8;
					warp_schedulers_per_sm = 2;
				break;
			}
		break;
		case 3:
			cuda_cores_per_sm = 192;
			fp_cores_per_sm = 32;
			warp_schedulers_per_sm = 4;
		break;
		case 5:
			cuda_cores_per_sm = 128;
			fp_cores_per_sm = 32;
			warp_schedulers_per_sm = 4;
		break;
	}
}

long gpu_info::get_threads_per_block() {
	return CUDA_WARP_SIZE;
}


#include <stdio.h>
#include "cuda_runtime.h"
#include "nbody.h"
#include "parallel_solver.h"
#include "kernel_header.h"
#include "gpu_info.h"

#ifndef GPU_NBODY_SOLVER_H
#define GPU_NBODY_SOLVER_H


class gpu_nbody_solver: public n_body, public parallel_solver
{
	//maybe add general methods to copy data from host to device and vice versa
public:
  gpu_nbody_solver(long n, long n_steps, long output_freq, float delta_t, particle* init, bool anim, long n_thr, mem_mode mmode);
    
  virtual void run() = 0;
    
  virtual ~gpu_nbody_solver();
protected:
  mem_mode mmode;
  particle* d_p;
  gpu_info* gpu;

	virtual void allocate_device_data() {
  		handle(cudaMalloc(&d_p, n * sizeof(particle)));
	}

  virtual void set_device_data() {
		  handle(cudaMemcpy(d_p, this->particles, n * sizeof(particle), cudaMemcpyHostToDevice));
  }

  virtual void get_device_data() {
      handle(cudaMemcpy(this->particles, d_p, n * sizeof(particle), cudaMemcpyDeviceToHost));
  }
    
  virtual void deallocate_device_data() {
  		handle(cudaFree(d_p));
  }

  virtual void wait_device() {
  	handle(cudaDeviceSynchronize());
  }

  inline virtual void handle(cudaError_t err) {
  	if(err != cudaSuccess) {
  		fprintf(stderr, "Cuda error(%d):%s\n", err, cudaGetErrorString(err));
  		exit(-1);
  	}
  }
};

#endif // GPU_NBODY_SOLVER_H

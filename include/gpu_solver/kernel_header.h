#include "nbody.h"
#include "gpu_info.h"



__global__ void global_reduced_iteration(long n, particle* particles, float_vector* forces, float delta_t);

__global__ void shmem_reduced_iteration(long n, particle* particles, float_vector* forces, float delta_t);

long int run_reduced_compute_forces(long n, particle* d_p, float_vector* d_f, float delta_t, mem_mode mmode, long threads_per_block);



__global__ void global_basic_iteration(long n, particle* particles, float delta_t);

__global__ void shmem_basic_iteration(long n, particle* particles, float delta_t);


long int run_basic_compute_forces(long n, particle* d_p, float delta_t, mem_mode mmode, long threads_per_block);
#ifndef CONFIGURATION_H_INCLUDED
#define CONFIGURATION_H_INCLUDED



//definition of execution modes
#define ANIMATION "animation"
#define EVALUATION "evaluation"
#define CHECK "check"

enum exec_mode {
    eval, anim, check
};

//definition of algorithm versions
#define NB_BASIC "basic"
#define NB_REDUCED "reduced"

enum solve_mode {
    basic, reduced
};

//definition of parallelization modes
#define SERIAL_NB_ARG "serial"
#define PTHREAD_NB_ARG "pthread"
#define OMP_NB_ARG "mp"
#define CUDA_NB_ARG "cuda"

enum parallel_mode {
    serial, pth, mp, cuda
};

//definition of memory access modes for gpu parallelization
#define CUDA_USE_GLOBAL "global"
#define CUDA_USE_SHARED "shmem"

enum mem_mode {
    global, shared
};

#define CUDA_MAX_THREADS_PER_BLOCK (1 << 10)

//number of tests run to make the average and the discard ratio
#define NUM_TESTS 10
#define DISCARD 0.2f


#define PTH_BLOCK 0
#define PTH_CYCLIC 1


#define DIM 2

#define CUDA_WARP_SIZE 32

#define X 0
#define Y 1
#define Z 2

#define WINDOW_NAME "corps"

#define SLEEP_TIME (0&100000)

#define D_G 6.673e-11
#define G 6.673e-11d

//define epsilon to avoid divisions by zero
#define EPS 1.0e-10d
#define D_EPS 1.0e-10

#define WEIGHT_FACTOR 0.0d
#define POS_FACTOR 0.0d
#define VEL_FACTOR 0.0d


#define SECOND 1.0d
#define MINUTE (60.0*SECOND)
#define HOUR (60.0*MINUTE)
#define DAY (24.0*HOUR)
#define MONTH (30*DAY)
#define YEAR (365.0*DAY)
#define MILLENIUM (1000.0*YEAR)
#define MILLION_YEARS (1000.0*MILLENIUM)


#define THOUSAND (1000.0)
#define MILLION (THOUSAND * THOUSAND)
#define BILLION (THOUSAND * MILLION)
#define TRILLION (THOUSAND * BILLION)
#define QUADRILLION (THOUSAND * TRILLION)
#define QUINTILLION (THOUSAND * QUADRILLION)
#define SEXTILLION (THOUSAND * QUINTILLION)
#define SEPTILLION (THOUSAND * SEXTILLION)


#define SCREEN_POS_FACTOR THOUSAND


#define TIME_STEP MINUTE



#define MONITOR_HEIGHT 900
#define MONITOR_WIDTH 1600

#define BODY_COLOR 0xE8D500


#endif // CONFIGURATION_H_INCLUDED

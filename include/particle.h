#include "configuration.h"

#include <cuda_runtime.h>

#ifndef PARTICLE_H
#define PARTICLE_H

struct float_vector {
    float data[DIM];
    __device__ __host__ float &operator[](int i) {
        return data[i];
    }
};


struct particle {
    float mass;
    float_vector pos;
    float_vector velocity;

    particle() {
        
    }

    particle(float mass, float_vector pos, float_vector velocity):
        mass(mass), pos(pos), velocity(velocity)
    {}
};

#endif // PARTICLE_H

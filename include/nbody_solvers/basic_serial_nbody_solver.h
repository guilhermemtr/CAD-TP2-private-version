#include "smp_basic_nbody_solver.h"

#ifndef BASIC_SERIAL_NBODY_SOLVER_H
#define BASIC_SERIAL_NBODY_SOLVER_H


class basic_serial_nbody_solver: public smp_basic_nbody_solver
{
public:
    basic_serial_nbody_solver(long n, long n_steps, long output_freq, float delta_t, particle* init, bool anim);
    virtual void run();
    virtual ~basic_serial_nbody_solver();
};

#endif // BASIC_SERIAL_NBODY_SOLVER_H

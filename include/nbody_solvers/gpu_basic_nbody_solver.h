#include "gpu_nbody_solver.h"
#include "kernel_header.h"

#ifndef GPU_BASIC_NBODY_SOLVER_H
#define GPU_BASIC_NBODY_SOLVER_H


class gpu_basic_nbody_solver: public gpu_nbody_solver
{
public:
    gpu_basic_nbody_solver(long n, long n_steps, long output_freq, float delta_t, particle* init, bool anim, long n_thr, mem_mode mmode);
    virtual void run();
    virtual ~gpu_basic_nbody_solver();
};

#endif // GPU_BASIC_NBODY_SOLVER_H

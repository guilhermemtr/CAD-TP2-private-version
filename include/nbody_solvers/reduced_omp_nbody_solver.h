#include "smp_reduced_nbody_solver.h"
#include "parallel_solver.h"

#include <omp.h>

#ifndef REDUCED_OMP_NBODY_SOLVER_H
#define REDUCED_OMP_NBODY_SOLVER_H


class reduced_omp_nbody_solver: public smp_reduced_nbody_solver, public parallel_solver
{
public:
    reduced_omp_nbody_solver(long n, long n_steps, long output_freq, float delta_t, particle* init, bool anim, long n_thr);
    virtual void run();
    virtual ~reduced_omp_nbody_solver();
protected:
    float_vector* loc_forces;
};

#endif // REDUCED_OMP_NBODY_SOLVER_H

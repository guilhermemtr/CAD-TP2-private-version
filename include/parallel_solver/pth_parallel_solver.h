#include "parallel_solver.h"

#include <pthread.h>

#ifndef PTH_PARALLEL_SOLVER_H
#define PTH_PARALLEL_SOLVER_H


typedef struct thr_args thr_args_t;

class pth_parallel_solver : public parallel_solver
{
public:
    pth_parallel_solver(long n_thr = 1);

    void loop_schedule(long my_rank, long thread_count, long n, long sched,
        long* first_p, long* last_p, long* incr_p);

    virtual void run() = 0;

    virtual void* work(void* rank) = 0;

    void barrier_init(void);

    void barrier(void);

    void barrier_destroy(void);

    virtual ~pth_parallel_solver();

protected:
    pthread_mutex_t b_mutex;
    pthread_cond_t b_cond_var;
    long b_thread_count;
    pthread_t* thread_handles;
    thr_args_t* thread_args;
};

 struct thr_args {
    pth_parallel_solver* obj;
    long rank;
};

#endif // PTH_PARALLEL_SOLVER_H

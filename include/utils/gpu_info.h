#include <math.h>
#include <time.h>
#include <iostream>
#include <stdio.h>
#include <stdexcept>
#include <stdlib.h>
#include <assert.h>
#include <cuda_runtime.h>

#include "configuration.h"

#ifndef __GPU_INFO__
#define __GPU_INFO__

class gpu_info {
public:
  	gpu_info();
    virtual ~gpu_info(){}
  	long get_threads_per_block();
private:
    void setup_threads_per_block(int major, int minor, int nr_sm);
  	
    long threads_per_block;
  	long cuda_cores_per_sm;
    long fp_cores_per_sm;
    long warp_schedulers_per_sm;



    inline virtual void handle(cudaError_t err) {
  	if(err != cudaSuccess) {
  		fprintf(stderr, "Cuda error(%d):%s\n", err, cudaGetErrorString(err));
  		exit(-1);
  	}
  }
};

#endif // __GPU_INFO__

#!/bin/bash

#Compilers and cleanup tools
CC=g++
RM=rm -f

NVCC=nvcc

#Source code and resulting's compilation file prefixes
CC_PREFIX=.cc
OBJ=.cc.o

CUDA_PREFIX=.cu
CUDA_OBJ=.cuo.o


GPU_ARCH=sm_52

#BE CAREFUL THIS VARIABLE IS INSTALATION DEPENDENT
#cuda paths
CUDA_INSTALL_PATH=/opt/cuda

#Source, include and library paths
SRC_PATH=src
INC=include include/nbody_solvers include/parallel_solver \
			include/smp_solver include/utils include/gpu_solver \
			$(CUDA_INSTALL_PATH)/include

LIBS=$(CUDA_INSTALL_PATH)/lib64

#Paths inclusions
INCLUDES=$(foreach d, $(INC), -I$d)

LIBRARIES=$(foreach d, $(LIBS), -L$d)


#Path that contains the various solvers implementation
SOLVERS_PATH=$(SRC_PATH)/nbody_solvers
SOLVER_SRC=basic_omp_nbody_solver.cc basic_pth_nbody_solver.cc basic_serial_nbody_solver.cc  \
		   gpu_basic_nbody_solver.cc gpu_reduced_nbody_solver.cc reduced_omp_nbody_solver.cc \
		   reduced_pth_nbody_solver.cc reduced_serial_nbody_solver.cc
SOLVER_SOURCES=$(foreach d, $(SOLVER_SRC), $(SOLVERS_PATH)/$d)


#Path that contains the various abstract parallel class utils
PARALLEL_PATH=$(SRC_PATH)/parallel_solver
PARALLEL_SRC=parallel_solver.cc pth_parallel_solver.cc
PARALLEL_SOURCES=$(foreach d, $(PARALLEL_SRC), $(PARALLEL_PATH)/$d)


#Path that contains the various abstract smp classes
SMP_SOLVER_PATH=$(SRC_PATH)/smp_solver
SMP_SRC=smp_basic_nbody_solver.cc smp_nbody_solver.cc smp_reduced_nbody_solver.cc
SMP_SOURCES=$(foreach d, $(SMP_SRC), $(SMP_SOLVER_PATH)/$d)

#Path that contains the various abstract gpu classes
GPU_SOLVER_PATH=$(SRC_PATH)/gpu_solver
GPU_SRC=gpu_nbody_solver.cc
GPU_SOURCES=$(foreach d, $(GPU_SRC), $(GPU_SOLVER_PATH)/$d)

#Path that contains the various util classes
UTILS_PATH=$(SRC_PATH)/utils
UTILS_SRC=statistics.cc time_lib.cc gpu_info.cc
UTILS_SOURCES=$(foreach d, $(UTILS_SRC), $(UTILS_PATH)/$d)


#Path that contains the various cuda files
CUDA_PATH=$(SOLVERS_PATH)/kernels
CUDA_SRC=basic.cu reduced.cu
CUDA_SOURCES=$(foreach d, $(CUDA_SRC), $(CUDA_PATH)/$d)



#Compilation and linking flags for $(CC)
CFLAGS=-std=c++11 -Wall $(MODE) $(INCLUDES) -fopenmp
LDFLAGS=-lpthread -lm -lX11 $(LIBRARIES) -lgomp -lcuda -lcudart

#NVCC flags
NVCCFLAGS=-I$(CUDA_INSTALL_PATH)/include -L$(CUDA_INSTALL_PATH)/lib64 \
	-arch=$(GPU_ARCH) -std=c++11 -Xcompiler -Wall $(MODE) \
	$(INCLUDES) -Xcompiler -fopenmp
NVCCLDFLAGS=-lgomp -lcuda -lcudart
NVCCOFLAGS=$(NVCC_MODE)

#Sources and corresponding objects for the source files
SOURCES=main.cc $(SOLVER_SOURCES) $(PARALLEL_SOURCES) \
				$(SMP_SOURCES) $(UTILS_SOURCES) $(GPU_SOURCES)
OBJECTS=$(SOURCES:.cc=.cc.o)

#Cuda objects to be compiled
CUDA_OBJECTS=$(CUDA_SOURCES:.cu=.cuo.o)


#Final executable program
EXECUTABLE=nbody_solver

#Mode to compile the code (either release or debug modes)
MODE=$(NO_OPTIMIZATION)
NVCC_MODE= $(FAST_MODE)

IEEE_754_COMPLIANCE=-ftz=false -prec-div=true -prec-sqrt=true
FAST_MODE=-ftz=true -prec-div=false -prec-sqrt=false

NO_OPTIMIZATION=-O0
RELEASE=-O2 -DNO_OUTPUT
DEBUG=-g
VERBOSE=-D__YAL_ON__ -DDEBUG
DEBUG_VERBOSE=$(DEBUG) $(VERBOSE)

all: $(SOURCES) $(EXECUTABLE)
	$(NVCC) $(NVCCFLAGS) $(OBJECTS) $(CUDA_OBJECTS) -o $(EXECUTABLE) $(LDFLAGS)

$(EXECUTABLE): $(OBJECTS) $(CUDA_OBJECTS)

%$(OBJ): %$(CC_PREFIX)
	$(NVCC) $(NVCCFLAGS) -c $< -o $@

%$(CUDA_OBJ): %$(CUDA_PREFIX)
	$(NVCC) $(NVCCOFLAGS) $(NVCCFLAGS) -c $? -o $@

clean:
	$(RM) $(OBJECTS) $(CUDA_OBJECTS) $(EXECUTABLE)
